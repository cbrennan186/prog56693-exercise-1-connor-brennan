using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using MongoDB.Driver;
using MongoDB.Bson;

namespace Complete
{
    public class DatabaseManager : MonoBehaviour
    {
        //Declare members
        private TankHealth tankHealth;
        private TankHealth tankHealth2;

        private TankShooting tankShooting;
        private TankAI tankAI;

        private TankMovement playerTankDis;

        private bool ifDead;
        private bool ifDead2;

        private float x1;
        private float y1;
        private float z1;

        private float x2;
        private float y2;
        private float z2;

        private float x3;
        private float y3;
        private float z3;

        private float x4;
        private float y4;
        private float z4;

        private int p_ShotCounter;
        private int ai_ShotCounter;

        private float RoundTimer;
        private int roundNum;

        private float aiDistance;
        private float playerDistance;

        private int roundsWon;

        private int shotsHit;

        private float shotAccuracy;

        //Declare the highsore members
        public class Highscore
        {
            [BsonId]
            public int id { get; set; }
            public string Username { get; set; }
            public float Accuracy { get; set; }
            public int TotalRoundsWon { get; set; }
        }

        //reads the sql database and prints it in the log
        private void ReadSQLDatabase()
        {
            string connectionString = "URI=file:" + Application.dataPath + "/_Completed-Assets/Resources/Database/Exercise1.db";
            using (IDbConnection dbConnection = new SqliteConnection(connectionString))
            {
                dbConnection.Open(); //Open connection to the database.

                IDbCommand command = dbConnection.CreateCommand();
                command.CommandText = "SELECT * FROM TankDestroyedLocation";

                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    float XAxis = reader.GetFloat(1);
                    float YAxis = reader.GetFloat(2);
                    float ZAxis = reader.GetFloat(3);

                    Debug.Log("Test:" + id + ", " + XAxis + ", " + YAxis + ", " + ZAxis);

                }

                reader.Close();
                command.Dispose();
                dbConnection.Close();
            }
        }

        //Write tank destructuion data to a table in sql
        private void WriteTankDesLocTable()
        {
            float _TankXPos;
            float _TankYPos;
            float _TankZPos;

            if (ifDead == true)
            {
                _TankXPos = tankHealth.GetTankPosX();
                _TankYPos = tankHealth.GetTankPosY();
                _TankZPos = tankHealth.GetTankPosZ();
            }
            else if(ifDead2 == true)
            {
                _TankXPos = tankHealth2.GetTankPosX();
                _TankYPos = tankHealth2.GetTankPosY();
                _TankZPos = tankHealth2.GetTankPosZ();
            }
            else
            {
                _TankXPos = 0;
                _TankYPos = 0;
                _TankZPos = 0;
            }

            string connectionString = "URI=file:" + Application.dataPath + "/_Completed-Assets/Resources/Database/Exercise1.db";
            using (IDbConnection connection = new SqliteConnection(connectionString))
            {
                connection.Open();
                //Open connection to the database.

                IDbCommand dbcmd = connection.CreateCommand();
                dbcmd.CommandText = "INSERT INTO TankDestroyedLocation(TankXAxis, TankYAxis, TankZAxis) VALUES (@TankXAxis, @TankYAxis, @TankZAxis)";

                IDbDataParameter Field1Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field1Parameter);

                Field1Parameter.ParameterName = "@TankXAxis";
                Field1Parameter.DbType = DbType.String;
                Field1Parameter.Value = _TankXPos;

                IDbDataParameter Field2Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field2Parameter);

                Field2Parameter.ParameterName = "@TankYAxis";
                Field2Parameter.DbType = DbType.String;
                Field2Parameter.Value = _TankYPos;

                IDbDataParameter Field3Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field3Parameter);

                Field3Parameter.ParameterName = "@TankZAxis";
                Field3Parameter.DbType = DbType.String;
                Field3Parameter.Value = _TankZPos;

                dbcmd.ExecuteNonQuery();

                dbcmd.Dispose();
                connection.Close();
            }
        }

        //Writes number of shots fired to a table in a database
        private void WriteTankShotsFiredTable()
        {
            string connectionString = "URI=file:" + Application.dataPath + "/_Completed-Assets/Resources/Database/Exercise1.db";
            using (IDbConnection connection = new SqliteConnection(connectionString))
            {
                connection.Open();
                //Open connection to the database.

                IDbCommand dbcmd = connection.CreateCommand();
                dbcmd.CommandText = "INSERT INTO TotalShotsFired(PlayerShots, AIShots) VALUES (@PlayerShots, @AIShots)";

                IDbDataParameter Field1Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field1Parameter);

                Field1Parameter.ParameterName = "@PlayerShots";
                Field1Parameter.DbType = DbType.String;
                Field1Parameter.Value = p_ShotCounter;

                IDbDataParameter Field2Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field2Parameter);

                Field2Parameter.ParameterName = "@AIShots";
                Field2Parameter.DbType = DbType.String;
                Field2Parameter.Value = ai_ShotCounter;

                dbcmd.ExecuteNonQuery();

                dbcmd.Dispose();
                connection.Close();
            }
        }

        ///Writes the round 1 data to a table in the database 
        private void WriteRound1DataTable()
        {
            string connectionString = "URI=file:" + Application.dataPath + "/_Completed-Assets/Resources/Database/Exercise1.db";
            using (IDbConnection connection = new SqliteConnection(connectionString))
            {
                connection.Open();
                //Open connection to the database.

                IDbCommand dbcmd = connection.CreateCommand();
                dbcmd.CommandText = "INSERT INTO Round1Data(TotalPlayerShots, TotalAIShots, TotalRoundTime, TotalPlayerDistance, TotalAIDistance) " +
                    "VALUES (@TotalPlayerShots, @TotalAIShots, @TotalRoundTime, @TotalPlayerDistance, @TotalAIDistance)";

                IDbDataParameter Field1Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field1Parameter);

                Field1Parameter.ParameterName = "@TotalPlayerShots";
                Field1Parameter.DbType = DbType.String;
                Field1Parameter.Value = p_ShotCounter;

                IDbDataParameter Field2Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field2Parameter);

                Field2Parameter.ParameterName = "@TotalAIShots";
                Field2Parameter.DbType = DbType.String;
                Field2Parameter.Value = ai_ShotCounter;

                IDbDataParameter Field3Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field3Parameter);

                Field3Parameter.ParameterName = "@TotalRoundTime";
                Field3Parameter.DbType = DbType.String;
                Field3Parameter.Value = RoundTimer;

                IDbDataParameter Field4Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field4Parameter);

                Field4Parameter.ParameterName = "@TotalPlayerDistance";
                Field4Parameter.DbType = DbType.String;
                Field4Parameter.Value = playerDistance;

                IDbDataParameter Field5Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field5Parameter);

                Field5Parameter.ParameterName = "@TotalAIDistance";
                Field5Parameter.DbType = DbType.String;
                Field5Parameter.Value = aiDistance;

                dbcmd.ExecuteNonQuery();

                dbcmd.Dispose();
                connection.Close();
            }
        }

        ///Writes the round 2 data to a table in the database 
        private void WriteRound2DataTable()
        {
            string connectionString = "URI=file:" + Application.dataPath + "/_Completed-Assets/Resources/Database/Exercise1.db";
            using (IDbConnection connection = new SqliteConnection(connectionString))
            {
                connection.Open();
                //Open connection to the database.

                IDbCommand dbcmd = connection.CreateCommand();
                dbcmd.CommandText = "INSERT INTO Round2Data(TotalPlayerShots, TotalAIShots, TotalRoundTime, TotalPlayerDistance, TotalAIDistance) " +
                    "VALUES (@TotalPlayerShots, @TotalAIShots, @TotalRoundTime, @TotalPlayerDistance, @TotalAIDistance)";

                IDbDataParameter Field1Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field1Parameter);

                Field1Parameter.ParameterName = "@TotalPlayerShots";
                Field1Parameter.DbType = DbType.String;
                Field1Parameter.Value = p_ShotCounter;

                IDbDataParameter Field2Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field2Parameter);

                Field2Parameter.ParameterName = "@TotalAIShots";
                Field2Parameter.DbType = DbType.String;
                Field2Parameter.Value = ai_ShotCounter;

                IDbDataParameter Field3Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field3Parameter);

                Field3Parameter.ParameterName = "@TotalRoundTime";
                Field3Parameter.DbType = DbType.String;
                Field3Parameter.Value = RoundTimer;

                IDbDataParameter Field4Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field4Parameter);

                Field4Parameter.ParameterName = "@TotalPlayerDistance";
                Field4Parameter.DbType = DbType.String;
                Field4Parameter.Value = playerDistance;

                IDbDataParameter Field5Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field5Parameter);

                Field5Parameter.ParameterName = "@TotalAIDistance";
                Field5Parameter.DbType = DbType.String;
                Field5Parameter.Value = aiDistance;

                dbcmd.ExecuteNonQuery();

                dbcmd.Dispose();
                connection.Close();
            }
        }

        ///Writes the round 3 data to a table in the database 
        private void WriteRound3DataTable()
        {
            string connectionString = "URI=file:" + Application.dataPath + "/_Completed-Assets/Resources/Database/Exercise1.db";
            using (IDbConnection connection = new SqliteConnection(connectionString))
            {
                connection.Open();
                //Open connection to the database.

                IDbCommand dbcmd = connection.CreateCommand();
                dbcmd.CommandText = "INSERT INTO Round3Data(TotalPlayerShots, TotalAIShots, TotalRoundTime, TotalPlayerDistance, TotalAIDistance) " +
                    "VALUES (@TotalPlayerShots, @TotalAIShots, @TotalRoundTime, @TotalPlayerDistance, @TotalAIDistance)";

                IDbDataParameter Field1Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field1Parameter);

                Field1Parameter.ParameterName = "@TotalPlayerShots";
                Field1Parameter.DbType = DbType.String;
                Field1Parameter.Value = p_ShotCounter;

                IDbDataParameter Field2Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field2Parameter);

                Field2Parameter.ParameterName = "@TotalAIShots";
                Field2Parameter.DbType = DbType.String;
                Field2Parameter.Value = ai_ShotCounter;

                IDbDataParameter Field3Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field3Parameter);

                Field3Parameter.ParameterName = "@TotalRoundTime";
                Field3Parameter.DbType = DbType.String;
                Field3Parameter.Value = RoundTimer;

                IDbDataParameter Field4Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field4Parameter);

                Field4Parameter.ParameterName = "@TotalPlayerDistance";
                Field4Parameter.DbType = DbType.String;
                Field4Parameter.Value = playerDistance;

                IDbDataParameter Field5Parameter = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(Field5Parameter);

                Field5Parameter.ParameterName = "@TotalAIDistance";
                Field5Parameter.DbType = DbType.String;
                Field5Parameter.Value = aiDistance;

                dbcmd.ExecuteNonQuery();

                dbcmd.Dispose();
                connection.Close();
            }
        }

        //Reads the data from the mongodb database for my specific data
        void ReadMongoDatabase()
        {
            MongoClient client = new MongoClient("mongodb+srv://GDAP-Student:jous.trok4POOS_pood@gdap2020-cluster.swxsa.mongodb.net/GDAP_Exercise?retryWrites=true&w=majority");
            var db = client.GetDatabase("GDAP_Exercise");
            var coll = db.GetCollection<Highscore>("Highscores").Find(u => u.Username == "Connor Brennan");

            if (coll.CountDocuments() > 0)
            {
                Highscore highscore = coll.Limit(1).Single();
                Debug.Log("User Data: " + highscore.id+ ", " + highscore.Username + ", " + highscore.Accuracy + ", " + highscore.TotalRoundsWon);
                //Debug.Log("Shot Accuracy: " + shotAccuracy + ", Rounds Won " + roundsWon);
            }
        }

        //Updates my record to the most recent data
        void UpdateMongoDatabase()
        {
            MongoClient client = new MongoClient("mongodb+srv://GDAP-Student:jous.trok4POOS_pood@gdap2020-cluster.swxsa.mongodb.net/GDAP_Exercise?retryWrites=true&w=majority");
            IMongoDatabase db = client.GetDatabase("GDAP_Exercise");
            var coll = db.GetCollection<Highscore>("Highscores").Find(u => u.Username == "Connor Brennan");

            if (coll.CountDocuments() > 0)
            {

                var score = db.GetCollection<Highscore>("Highscores");

                var filter = Builders<Highscore>.Filter.Eq("Username", "Connor Brennan");
                var update = Builders<Highscore>.Update.Set("Accuracy", shotAccuracy);

                var updateOptions = new UpdateOptions()
                {
                    IsUpsert = true
                };

                score.UpdateOne(filter, update, updateOptions);

                var update2 = Builders<Highscore>.Update.Set("TotalRoundsWon", roundsWon);

                score.UpdateOne(filter, update2, updateOptions);
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            x1 = 0;
            y1 = 0;
            z1 = 0;

            x2 = 0;
            y2 = 0;
            z2 = 0;

            x3 = 0;
            y3 = 0;
            z3 = 0;

            x4 = 0;
            y4 = 0;
            z4 = 0;

            tankHealth = GameObject.FindGameObjectWithTag("TankTag").GetComponent<TankHealth>();
            tankHealth2 = GameObject.FindGameObjectWithTag("TankTag2").GetComponent<TankHealth>();

            tankShooting = GameObject.FindGameObjectWithTag("TankTag").GetComponent<TankShooting>();
            tankAI = GameObject.FindGameObjectWithTag("TankTag2").GetComponent<TankAI>();

            playerTankDis = GameObject.FindGameObjectWithTag("TankTag").GetComponent<TankMovement>();

            p_ShotCounter = 0;
            ai_ShotCounter = 0;

            RoundTimer = 0;
            roundNum = 1;

            roundsWon = 0;

            aiDistance = 0;
            playerDistance = 0;

            shotsHit = 0;
            shotAccuracy = 0;
        }

        // Update is called once per frame
        void Update()
        {
            shotsHit = tankHealth.GetShotHit();

            ifDead = tankHealth.GetDead();
            ifDead2 = tankHealth2.GetDead();

            x1 = tankHealth.GetTankPosX();
            y1 = tankHealth.GetTankPosY();
            z1 = tankHealth.GetTankPosZ();

            x3 = tankHealth2.GetTankPosX();
            y3 = tankHealth2.GetTankPosY();
            z3 = tankHealth2.GetTankPosZ();

            p_ShotCounter = tankShooting.GetNumShotsFired();
            ai_ShotCounter = tankAI.GetNumShotsFired();

            aiDistance = tankAI.GetDistance();
            playerDistance = playerTankDis.GetDistance();

            //If neither tank has died, collect the time it takes for the round to end
            if (ifDead == false && ifDead2 == false && roundNum == 1)
            {
                RoundTimer += Time.deltaTime;
            }
            else if (ifDead == false && ifDead2 == false && roundNum == 2)
            {
                RoundTimer += Time.deltaTime;
            }
            else if (ifDead == false && ifDead2 == false && roundNum == 3)
            {
                RoundTimer += Time.deltaTime;
            }

            //If either tank is destoryed collect the data from the round, and ensure it is the correct round
            if (ifDead == true && x1 != x2 && y1 != y2 && z1 != z2 && roundNum == 1
                || ifDead2 == true && x3 != x4 && y3 != y4 && z3 != z4 && roundNum == 1)
            {
                if (ifDead2 == true)
                {
                    roundsWon = roundsWon + 1;
                }

                shotAccuracy = p_ShotCounter / shotsHit;

                WriteRound1DataTable();
                WriteTankDesLocTable();
                WriteTankShotsFiredTable();
                UpdateMongoDatabase();
                ReadMongoDatabase();

                ifDead = false;
                ifDead2 = false;

                roundNum = roundNum + 1;
                RoundTimer = 0;

                aiDistance = 0;
                playerDistance = 0;


                x2 = tankHealth.GetTankPosX();
                y2 = tankHealth.GetTankPosY();
                z2 = tankHealth.GetTankPosZ();

                x4 = tankHealth2.GetTankPosX();
                y4 = tankHealth2.GetTankPosY();
                z4 = tankHealth2.GetTankPosZ();
            }

            else if (ifDead == true && x1 != x2 && y1 != y2 && z1 != z2 && roundNum == 2
                || ifDead2 == true && x3 != x4 && y3 != y4 && z3 != z4 && roundNum == 2)
            {
                if (ifDead2 == true)
                {
                    roundsWon = roundsWon + 1;
                }

                shotAccuracy = p_ShotCounter / shotsHit;

                WriteRound1DataTable();
                WriteTankDesLocTable();
                WriteTankShotsFiredTable();
                UpdateMongoDatabase();
                ReadMongoDatabase();

                ifDead = false;
                ifDead2 = false;

                roundNum = roundNum + 1;
                RoundTimer = 0;

                aiDistance = 0;
                playerDistance = 0;

                x2 = tankHealth.GetTankPosX();
                y2 = tankHealth.GetTankPosY();
                z2 = tankHealth.GetTankPosZ();

                x4 = tankHealth2.GetTankPosX();
                y4 = tankHealth2.GetTankPosY();
                z4 = tankHealth2.GetTankPosZ();
            }

            else if (ifDead == true && x1 != x2 && y1 != y2 && z1 != z2 && roundNum == 3
                || ifDead2 == true && x3 != x4 && y3 != y4 && z3 != z4 && roundNum == 3)
            {
                if (ifDead2 == true)
                {
                    roundsWon = roundsWon + 1;
                }

                shotAccuracy = p_ShotCounter / shotsHit;

                WriteRound1DataTable();
                WriteTankDesLocTable();
                WriteTankShotsFiredTable();
                UpdateMongoDatabase();
                ReadMongoDatabase();

                ifDead = false;
                ifDead2 = false;

                roundNum = roundNum + 1;
                RoundTimer = 0;

                aiDistance = 0;
                playerDistance = 0;

                x2 = tankHealth.GetTankPosX();
                y2 = tankHealth.GetTankPosY();
                z2 = tankHealth.GetTankPosZ();

                x4 = tankHealth2.GetTankPosX();
                y4 = tankHealth2.GetTankPosY();
                z4 = tankHealth2.GetTankPosZ();

            }

            else if (ifDead == true && x1 != x2 && y1 != y2 && z1 != z2 && roundNum >= 4
                || ifDead2 == true && x3 != x4 && y3 != y4 && z3 != z4 && roundNum >= 4)
            {
                if (ifDead2 == true)
                {
                    roundsWon = roundsWon + 1;
                }

                shotAccuracy = p_ShotCounter / shotsHit;

                WriteTankDesLocTable();
                WriteTankShotsFiredTable();
                UpdateMongoDatabase();
                ReadMongoDatabase();

                ifDead = false;
                ifDead2 = false;

                roundNum = roundNum + 1;
                RoundTimer = 0;

                aiDistance = 0;
                playerDistance = 0;

                x2 = tankHealth.GetTankPosX();
                y2 = tankHealth.GetTankPosY();
                z2 = tankHealth.GetTankPosZ();

                x4 = tankHealth2.GetTankPosX();
                y4 = tankHealth2.GetTankPosY();
                z4 = tankHealth2.GetTankPosZ();
            }
        }
    }
}
