﻿using System;
using System.Windows;
using System.Data;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Windows.Controls;

namespace SQLDatabaseWPFForm
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    
    //Main class that runs on start, loading all nessasary data and running the program
    public partial class MainWindow : Window
    {
        string databaseNameString = "";
        string SelectedTable = "";
        public MainWindow()
        {
            InitializeComponent();

            DatabaseComboBox.Items.Add("Round1Data");
            DatabaseComboBox.Items.Add("Round2Data");
            DatabaseComboBox.Items.Add("Round3Data");
            DatabaseComboBox.Items.Add("TankDestroyedLocation");
            DatabaseComboBox.Items.Add("TotalShotsFired");
        }

        //Method to load in the TotalShotsFired data
        private void LoadTotalShotsFired()
        {
            SQLiteConnection conn = new SQLiteConnection(@"Data Source=C:/Users/cbren/Desktop/Game Programs/Assignment 1 data and tools/Assets/_Completed-Assets/Resources/Database/"+databaseNameString+".db; ");
            DataTable dataTable = new DataTable();
            SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter("SELECT * FROM TotalShotsFired", conn);

            dataAdapter.Fill(dataTable);

            foreach(DataRow dataRow in dataTable.Rows)
            {
                ShotList.Items.Add("ShotIDs: "+ dataRow["ShotID"] + " PlayerShots: "+dataRow["PlayerShots"].ToString()+" AI Shots: "+dataRow["AIShots"].ToString());
            }
        }

        //Method to load in the TankDestroyedLocation data
        private void LoadTankDestroyedLocation()
        {
            SQLiteConnection conn = new SQLiteConnection(@"Data Source=C:/Users/cbren/Desktop/Game Programs/Assignment 1 data and tools/Assets/_Completed-Assets/Resources/Database/" + databaseNameString + ".db; ");
            DataTable dataTable = new DataTable();
            SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter("SELECT * FROM TankDestroyedLocation", conn);

            dataAdapter.Fill(dataTable);

            foreach (DataRow dataRow in dataTable.Rows)
            {
                ShotList.Items.Add("TankDestroyedIDs: " + dataRow["TankDestroyedID"].ToString() + " TankXAxis: " + dataRow["TankXAxis"].ToString() + " TankYAxis: " + dataRow["TankYAxis"].ToString() + " TankZAxis: " + dataRow["TankZAxis"].ToString());
            }
        }

        //Method to load in the Round1Data data
        private void LoadRound1Data()
        {
            SQLiteConnection conn = new SQLiteConnection(@"Data Source=C:/Users/cbren/Desktop/Game Programs/Assignment 1 data and tools/Assets/_Completed-Assets/Resources/Database/" + databaseNameString + ".db; ");
            DataTable dataTable = new DataTable();
            SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter("SELECT * FROM Round1Data", conn);

            dataAdapter.Fill(dataTable);

            foreach (DataRow dataRow in dataTable.Rows)
            {
                ShotList.Items.Add("RoundID: " + dataRow["RoundID"].ToString() + " TotalPlayerShots: " + dataRow["TotalPlayerShots"].ToString() + " TotalAIShots: " + dataRow["TotalAIShots"].ToString() + " TotalRoundTime: " + dataRow["TotalRoundTime"].ToString() + " TotalPlayerDistance: " + dataRow["TotalPlayerDistance"].ToString() + " TotalAIDistance: " + dataRow["TotalAIDistance"].ToString());
            }
        }

        //Method to load in the Round2Data data
        private void LoadRound2Data()
        {
            SQLiteConnection conn = new SQLiteConnection(@"Data Source=C:/Users/cbren/Desktop/Game Programs/Assignment 1 data and tools/Assets/_Completed-Assets/Resources/Database/" + databaseNameString + ".db; ");
            DataTable dataTable = new DataTable();
            SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter("SELECT * FROM Round2Data", conn);

            dataAdapter.Fill(dataTable);

            foreach (DataRow dataRow in dataTable.Rows)
            {
                ShotList.Items.Add("RoundID: " + dataRow["RoundID"].ToString() + " TotalPlayerShots: " + dataRow["TotalPlayerShots"].ToString() + " TotalAIShots: " + dataRow["TotalAIShots"].ToString() + " TotalRoundTime: " + dataRow["TotalRoundTime"].ToString() + " TotalPlayerDistance: " + dataRow["TotalPlayerDistance"].ToString() + " TotalAIDistance: " + dataRow["TotalAIDistance"].ToString());
            }
        }

        //Method to load in the Round3Data data
        private void LoadRound3Data()
        {
            SQLiteConnection conn = new SQLiteConnection(@"Data Source=C:/Users/cbren/Desktop/Game Programs/Assignment 1 data and tools/Assets/_Completed-Assets/Resources/Database/" + databaseNameString + ".db; ");
            DataTable dataTable = new DataTable();
            SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter("SELECT * FROM Round3Data", conn);

            dataAdapter.Fill(dataTable);

            foreach (DataRow dataRow in dataTable.Rows)
            {
                ShotList.Items.Add("RoundID: " + dataRow["RoundID"].ToString() + " TotalPlayerShots: " + dataRow["TotalPlayerShots"].ToString() + " TotalAIShots: " + dataRow["TotalAIShots"].ToString() + " TotalRoundTime: " + dataRow["TotalRoundTime"].ToString() + " TotalPlayerDistance: " + dataRow["TotalPlayerDistance"].ToString() + " TotalAIDistance: " + dataRow["TotalAIDistance"].ToString());
            }
        }

        //Method that on a click of the button, Displays the choosen data from the combo box,
        //and clears out the old data from the list
        private void ShotButton_Click(object sender, RoutedEventArgs e)
        {
            SelectedTable = DatabaseComboBox.SelectedValue.ToString();
            if(SelectedTable == "TotalShotsFired")
            {
                ShotList.Items.Clear();
                LoadTotalShotsFired();
            }
            else if(SelectedTable == "TankDestroyedLocation")
            {
                ShotList.Items.Clear();
                LoadTankDestroyedLocation();
            }
            else if (SelectedTable == "Round3Data")
            {
                ShotList.Items.Clear();
                LoadRound3Data();
            }
            else if (SelectedTable == "Round2Data")
            {
                ShotList.Items.Clear();
                LoadRound2Data();
            }
            else if (SelectedTable == "Round1Data")
            {
                ShotList.Items.Clear();
                LoadRound1Data();
            }
        }

        //Gets and loads in the selected database file by name
        private void DatabaseName_Button_Click(object sender, RoutedEventArgs e)
        {
            databaseNameString = DatabaseNameText.Text;
        }
    }
}
